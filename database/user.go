package database

import (
	"context"

	"gitlab.com/feidtmb/notes/domain"
)

// UserDB is the interface for persisting users.
type UserDB interface {
	// CreateUser creates a new user.
	CreateUser(context.Context, NewUser) error

	// GetUser retrieves a user.
	GetUser(ctx context.Context, email string) (domain.User, error)

	// ChangeUserEmail changes a user's email address. It must also set the
	// reauthenticate time to the current time.
	ChangeUserEmail(context.Context, ChangedUserEmail) error

	// ChangeUserPassword changes a user's password. It must also set the
	// reauthenticate time to the current time.
	ChangeUserPassword(context.Context, ChangedUserPassword) error
}

// NewUser is the required data for a new user.
type NewUser struct {
	Email        string
	PasswordHash string
}

// ChangedUserEmail is the required data to change a user's email.
type ChangedUserEmail struct {
	Email    string
	NewEmail string
}

// ChangedUserPassword is the required data to change a user's password.
type ChangedUserPassword struct {
	Email        string
	PasswordHash string
}
