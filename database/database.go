// Package database defines a database interface.
package database

// DB is a database interface.
type DB interface {
	UserDB
	NoteDB
	KeyDB
}
