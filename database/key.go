package database

import (
	"context"

	"gitlab.com/feidtmb/lib/crypt"
)

// KeyDB is the interface for persisting cryptographic keys used by the server.
type KeyDB interface {
	// GetKey retrieves a key.
	GetKey(ctx context.Context, id string) (crypt.SymmetricKey, error)

	// GetFreshKey retrieves a key that won't expire soon, along with its ID. If
	// no such key exists, one is created.
	GetFreshKey(ctx context.Context) (key crypt.SymmetricKey, id string, err error)

	// DeleteExpiredKeys deletes expired keys.
	DeleteExpiredKeys(ctx context.Context) error
}
