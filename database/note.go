package database

import (
	"context"

	"gitlab.com/feidtmb/notes/domain"
)

// NoteDB is the interface for persisting notes.
type NoteDB interface {
	// CreateNote creates a new note.
	//
	// Newly created notes automatically have highest sort priority among the
	// related user's notes.
	CreateNote(context.Context, NewNote) (id string, err error)

	// GetNote retrieves a note.
	GetNote(ctx context.Context, id string) (domain.Note, error)

	// SearchNotes retrieves multiple notes belonging to a user.
	SearchNotes(ctx context.Context, userEmail string) ([]domain.Note, error)

	// ChangeNoteContent changes a note's content.
	ChangeNoteContent(context.Context, ChangedNoteContent) error

	// ChangeNotePriority changes a note's sort priority relative to the related
	// user's other notes.
	//
	// If the note's sort priority is already highest or lowest when attempting
	// to change up or down respectively, then no error is returned.
	ChangeNotePriority(context.Context, ChangedNotePriority) error

	// DeleteNote deletes a note.
	DeleteNote(ctx context.Context, id string) error
}

// NewNote is the required data for a new note.
type NewNote struct {
	UserEmail string
	Content   string
}

// ChangedNoteContent is the required data to change a note's content.
type ChangedNoteContent struct {
	ID      string
	Content string
}

// ChangedNotePriority is the required data to change a note's priority.
type ChangedNotePriority struct {
	ID             string
	PriorityChange NotePriorityChange
}

// NotePriorityChange is a change to a note's sort priority relative to other
// notes belonging to the same user.
type NotePriorityChange string

// Relative changes to note sort priority.
const (
	// NotePriorityUp indicates that a note's priority should change such that
	// it swaps sort positions with the note that comes before it in the sorted
	// list.
	NotePriorityUp NotePriorityChange = "up"

	// NotePriorityDown indicates that a note's priority should change such that
	// it swaps sort positions with the note that comes after it in the sorted
	// list.
	NotePriorityDown NotePriorityChange = "down"

	// NotePriorityHighest indicates that a note's priority should change such
	// that it becomes the first note in the sorted list.
	NotePriorityHighest NotePriorityChange = "highest"

	// NotePriorityLowest indicates that a note's priority should change such
	// that it becomes the last note in the sorted list.
	NotePriorityLowest NotePriorityChange = "lowest"
)
