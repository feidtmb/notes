package mem

import (
	"context"
	"time"

	"github.com/pborman/uuid"

	"gitlab.com/feidtmb/lib/crypt"
	"gitlab.com/feidtmb/notes/domain"
)

type Key struct {
	k       crypt.SymmetricKey
	expires time.Time
}

func (db *DB) GetKey(ctx context.Context, id string) (crypt.SymmetricKey, error) {
	key, ok := db.keyMap[id]
	if !ok || time.Now().After(key.expires) {
		return crypt.SymmetricKey{}, domain.Err{Kind: domain.ErrNotFound}
	}

	return key.k, nil
}

func (db *DB) GetFreshKey(ctx context.Context) (key crypt.SymmetricKey, id string, err error) {
	var (
		freshest   Key
		freshestID string
	)
	for id, k := range db.keyMap {
		if k.expires.After(freshest.expires) {
			freshest = k
			freshestID = id
		}
	}

	if freshestID == "" || time.Until(freshest.expires) < time.Hour*24*30 {
		freshest.k = crypt.NewSymmetricKey()
		freshest.expires = time.Now().Add(time.Hour * 24 * 90)
		freshestID = uuid.New()
		db.keyMap[freshestID] = freshest
	}

	return freshest.k, freshestID, nil
}

func (db *DB) DeleteExpiredKeys(ctx context.Context) error {
	now := time.Now()
	for id, k := range db.keyMap {
		if now.After(k.expires) {
			delete(db.keyMap, id)
		}
	}

	return nil
}
