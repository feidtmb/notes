package mem

import (
	"context"
	"errors"
	"time"

	"gitlab.com/feidtmb/notes/database"
	"gitlab.com/feidtmb/notes/domain"
)

func (db *DB) CreateUser(ctx context.Context, new database.NewUser) error {
	if _, ok := db.userMap[new.Email]; ok {
		return domain.Err{
			Kind:    domain.ErrRequest,
			Message: "A user with the chosen email already exists.",
			Cause:   errors.New("user email must be unique"),
		}
	}

	user := domain.User{
		Email:        new.Email,
		PasswordHash: new.PasswordHash,
		Created:      domain.NewTimestamp(time.Now()),
	}

	db.userMap[new.Email] = user

	return nil
}

func (db *DB) GetUser(ctx context.Context, email string) (domain.User, error) {
	user, ok := db.userMap[email]
	if !ok {
		return domain.User{}, domain.Err{Kind: domain.ErrNotFound}
	}

	return user, nil
}

func (db *DB) ChangeUserEmail(ctx context.Context, changed database.ChangedUserEmail) error {
	if _, ok := db.userMap[changed.NewEmail]; ok {
		return domain.Err{
			Kind:    domain.ErrRequest,
			Message: "A user with the chosen email already exists.",
			Cause:   errors.New("user email must be unique"),
		}
	}

	user, ok := db.userMap[changed.Email]
	if !ok {
		return domain.Err{Kind: domain.ErrNotFound}
	}

	for key, note := range db.noteMap {
		if note.UserEmail == user.Email {
			note.UserEmail = changed.NewEmail
			db.noteMap[key] = note
		}
	}

	user.Email = changed.NewEmail
	user.Reauthenticate = time.Now()

	delete(db.userMap, changed.Email)
	db.userMap[changed.NewEmail] = user
	return nil
}

func (db *DB) ChangeUserPassword(ctx context.Context, changed database.ChangedUserPassword) error {
	user, ok := db.userMap[changed.Email]
	if !ok {
		return domain.Err{Kind: domain.ErrNotFound}
	}

	user.PasswordHash = changed.PasswordHash
	user.Reauthenticate = time.Now()

	db.userMap[changed.Email] = user
	return nil
}
