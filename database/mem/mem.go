// Package mem implements a database in memory.
//
// Intended for use in testing only.
package mem

import (
	"context"

	"gitlab.com/feidtmb/notes/database"
	"gitlab.com/feidtmb/notes/domain"
)

type DB struct {
	userMap map[string]domain.User
	noteMap map[string]domain.Note
	keyMap  map[string]Key
}

type Seed struct {
	User  database.NewUser
	Notes []database.NewNote
}

func New(seed *Seed) (*DB, error) {
	db := &DB{
		userMap: make(map[string]domain.User),
		noteMap: make(map[string]domain.Note),
		keyMap:  make(map[string]Key),
	}

	if seed == nil {
		return db, nil
	}

	ctx := context.Background()
	if err := db.CreateUser(ctx, seed.User); err != nil {
		return nil, err
	}

	for _, note := range seed.Notes {
		if _, err := db.CreateNote(ctx, note); err != nil {
			return nil, err
		}
	}

	return db, nil
}
