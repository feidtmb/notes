package mem

import (
	"context"
	"fmt"
	"time"

	"github.com/pborman/uuid"
	"gitlab.com/feidtmb/notes/database"
	"gitlab.com/feidtmb/notes/domain"
)

func (db *DB) CreateNote(ctx context.Context, new database.NewNote) (string, error) {
	// Calculate the new note's priority.
	var priority int
	for _, n := range db.noteMap {
		if n.Priority >= priority {
			priority = n.Priority + 1
		}
	}

	note := domain.Note{
		ID:        uuid.New(),
		UserEmail: new.UserEmail,
		Content:   new.Content,
		Priority:  priority,
		Created:   domain.NewTimestamp(time.Now()),
	}

	db.noteMap[note.ID] = note

	return note.ID, nil
}

func (db *DB) GetNote(ctx context.Context, id string) (domain.Note, error) {
	note, ok := db.noteMap[id]
	if !ok {
		return domain.Note{}, domain.Err{Kind: domain.ErrNotFound}
	}

	return note, nil
}

func (db *DB) SearchNotes(ctx context.Context, userEmail string) ([]domain.Note, error) {
	// Use a literal so that we return a non-nil slice if nothing is found.
	notes := []domain.Note{}

	for _, note := range db.noteMap {
		if note.UserEmail == userEmail {
			notes = append(notes, note)
		}
	}

	return notes, nil
}

func (db *DB) ChangeNoteContent(ctx context.Context, changed database.ChangedNoteContent) error {
	note, ok := db.noteMap[changed.ID]
	if !ok {
		return domain.Err{Kind: domain.ErrNotFound}
	}

	note.Content = changed.Content
	note.Modified = domain.NewTimestamp(time.Now())
	db.noteMap[changed.ID] = note

	return nil
}

func (db *DB) ChangeNotePriority(ctx context.Context, changed database.ChangedNotePriority) error {
	note, ok := db.noteMap[changed.ID]
	if !ok {
		return domain.Err{Kind: domain.ErrNotFound}
	}

	// If this is the only note, then changing priority is a no-op.
	if len(db.noteMap) == 1 {
		return nil
	}

	// Get IDs of all notes with priority values that could be needed depending
	// on how we're adjusting priority.
	highest := note
	lowest := note
	var nextHighest, nextLowest *domain.Note
	for _, n := range db.noteMap {
		// Don't consider the note that we're changing the priority of.
		if n.ID == note.ID {
			continue
		}

		// Adjust highest and lowest if needed.
		if n.Priority > highest.Priority {
			highest = n
		}
		if n.Priority < lowest.Priority {
			lowest = n
		}

		// Adjust next highest and lowest if needed.
		if n.Priority > note.Priority && (nextHighest == nil || nextHighest.Priority > n.Priority) {
			copy := n
			nextHighest = &copy
		}
		if n.Priority < note.Priority && (nextLowest == nil || nextLowest.Priority < n.Priority) {
			copy := n
			nextLowest = &copy
		}
	}

	switch changed.PriorityChange {
	case database.NotePriorityUp:
		if nextHighest == nil {
			// Already highest.
			return nil
		}

		// Swap priorities with the next highest.
		p := note.Priority
		note.Priority = nextHighest.Priority
		nextHighest.Priority = p

		// Save changes.
		db.noteMap[nextHighest.ID] = *nextHighest
		db.noteMap[note.ID] = note
		return nil
	case database.NotePriorityDown:
		if nextLowest == nil {
			// Already lowest.
			return nil
		}

		// Swap priorities with the next lowest.
		p := note.Priority
		note.Priority = nextLowest.Priority
		nextLowest.Priority = p

		// Save changes.
		db.noteMap[nextLowest.ID] = *nextLowest
		db.noteMap[note.ID] = note
		return nil
	case database.NotePriorityHighest:
		note.Priority = highest.Priority + 1
		db.noteMap[note.ID] = note
		return nil
	case database.NotePriorityLowest:
		note.Priority = lowest.Priority - 1
		db.noteMap[note.ID] = note
		return nil
	default:
		return domain.Err{
			Kind:  domain.ErrInternal,
			Cause: fmt.Errorf("unrecognized note priority change: %q", changed.PriorityChange),
		}
	}
}

func (db *DB) DeleteNote(ctx context.Context, id string) error {
	delete(db.noteMap, id)
	return nil
}
