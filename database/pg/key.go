package pg

import (
	"context"
	"database/sql"
	"time"

	"github.com/pborman/uuid"
	"gitlab.com/feidtmb/lib/crypt"
	"gitlab.com/feidtmb/notes/domain"
)

// GetKey retrieves a key.
func (db *DB) GetKey(ctx context.Context, id string) (crypt.SymmetricKey, error) {
	var (
		key     string
		expires time.Time
	)
	if err := db.db.QueryRowContext(ctx, `
		SELECT key, expires
		FROM keys
		WHERE id = $1;
	`, id).Scan(&key, &expires); err != nil {
		if err == sql.ErrNoRows {
			return crypt.SymmetricKey{}, domain.Err{Kind: domain.ErrNotFound}
		}

		return crypt.SymmetricKey{}, err
	}

	if time.Now().After(expires) {
		// Key is expired, treat as though it doesn't exist.
		return crypt.SymmetricKey{}, domain.Err{Kind: domain.ErrNotFound}
	}

	var k crypt.SymmetricKey
	err := k.UnmarshalText([]byte(key))
	return k, err
}

// GetFreshKey retrieves a key that won't expire soon, along with its ID. If no
// such key exists, one is created.
func (db *DB) GetFreshKey(ctx context.Context) (key crypt.SymmetricKey, id string, err error) {
	var (
		k       string
		expires time.Time
	)
	if err := db.db.QueryRowContext(ctx, `
		SELECT id, key, expires
		FROM keys
		ORDER BY expires DESC
		LIMIT 1;
	`).Scan(&id, &k, &expires); err != nil {
		// If no key is found, then we'll leave expires as the zero time and
		// fall through to creating an unexpired key.
		if err != sql.ErrNoRows {
			return crypt.SymmetricKey{}, "", err
		}
		// Defensively guarantee expires is still the zero time.
		expires = time.Time{}
	}

	if time.Until(expires) < time.Hour*24*30 {
		// We don't have a fresh key (one that won't expire for at least 30
		// days), so we'll create one.
		id = uuid.New()
		key = crypt.NewSymmetricKey()
		expires = time.Now().Add(time.Hour * 24 * 90) // 90 days.
		if _, err := db.db.ExecContext(ctx, `
			INSERT INTO keys (
				id, key, expires
			) VALUES (
				$1, $2, $3
			);
		`, id, key.String(), expires); err != nil {
			return crypt.SymmetricKey{}, "", err
		}
		return key, id, nil
	}

	err = key.UnmarshalText([]byte(k))
	return key, id, err
}

// DeleteExpiredKeys deletes expired keys.
func (db *DB) DeleteExpiredKeys(ctx context.Context) error {
	_, err := db.db.ExecContext(ctx, `
		DELETE FROM keys
		WHERE expires <= CURRENT_TIMESTAMP;
	`)
	return err
}
