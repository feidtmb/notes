// Package pg implements a database using PostgreSQL.
package pg

import (
	"database/sql"

	_ "github.com/lib/pq" // driver
)

type DB struct {
	db *sql.DB
}

func New(connStr string) (*DB, error) {
	db, err := sql.Open("postgres", connStr)
	return &DB{db: db}, err
}

func (db *DB) Close() {
	db.db.Close()
}
