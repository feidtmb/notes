package pg

import (
	"context"
	"database/sql"
	"errors"
	"time"

	"github.com/lib/pq"
	"gitlab.com/feidtmb/notes/database"
	"gitlab.com/feidtmb/notes/domain"
)

func (db *DB) CreateUser(ctx context.Context, new database.NewUser) error {
	_, err := db.db.ExecContext(ctx, `
		INSERT INTO users (
			email, password_hash
		) VALUES (
			$1, $2
		);
	`, new.Email, new.PasswordHash)

	// Translate error if user with email already exists.
	if isUserEmailUniqueConstraintErr(err) {
		return domain.Err{
			Kind:    domain.ErrRequest,
			Message: "A user with the chosen email already exists.",
			Cause:   errors.New("user email must be unique"),
		}
	}

	return err
}

func (db *DB) GetUser(ctx context.Context, email string) (domain.User, error) {
	u := domain.User{Email: email}
	var (
		reauthenticate sql.NullTime
		created        time.Time
	)

	if err := db.db.QueryRowContext(ctx, `
		SELECT password_hash, reauthenticate, created FROM users WHERE email = $1;
	`, email).Scan(&u.PasswordHash, &reauthenticate, &created); err != nil {
		if err == sql.ErrNoRows {
			return domain.User{}, domain.Err{Kind: domain.ErrNotFound}
		}

		return domain.User{}, err
	}

	if reauthenticate.Valid {
		u.Reauthenticate = reauthenticate.Time
	}
	u.Created = domain.NewTimestamp(created)

	return u, nil
}

func (db *DB) ChangeUserEmail(ctx context.Context, changed database.ChangedUserEmail) error {
	_, err := db.db.ExecContext(ctx, `
		UPDATE users SET
			email = $1,
			reauthenticate = $2
		WHERE
			email = $3;
	`, changed.NewEmail, time.Now(), changed.Email)

	// Translate error if user with new email already exists.
	if isUserEmailUniqueConstraintErr(err) {
		return domain.Err{
			Kind:    domain.ErrRequest,
			Message: "A user with the chosen email already exists.",
			Cause:   errors.New("user email must be unique"),
		}
	}

	return err
}

func (db *DB) ChangeUserPassword(ctx context.Context, changed database.ChangedUserPassword) error {
	_, err := db.db.ExecContext(ctx, `
		UPDATE users SET
			password_hash = $1,
			reauthenticate = $2
		WHERE
			email = $3;
	`, changed.PasswordHash, time.Now(), changed.Email)

	return err
}

// Returns true if err is related to a user email not being unique when it's
// required to be.
func isUserEmailUniqueConstraintErr(err error) bool {
	if err == nil {
		return false
	}

	pqErr, ok := err.(*pq.Error)
	if !ok {
		return false
	}

	// Error is from our driver (lib/pq), meaning we can inspect it to find out
	// if it's related to the UNIQUE email constraint.
	//
	// There are two things we'll check:
	//   1. The error code.
	//   2. The violated constraint name.

	// Error code should be related to a unique violation.
	if pqErr.Code != pq.ErrorCode(ErrCodeUniqueViolation) {
		return false
	}

	// Violated constraint should be the users primary key constraint.
	return pqErr.Constraint == "users_pkey"
}
