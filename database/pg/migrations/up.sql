CREATE TABLE IF NOT EXISTS users (
    email          text        PRIMARY KEY CHECK (email != ''),
    password_hash  text        NOT NULL    CHECK (password_hash != ''),
    reauthenticate timestamptz,
    created        timestamptz NOT NULL    DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS notes (
    id         uuid        PRIMARY KEY,
    user_email text        NOT NULL REFERENCES users (email) ON DELETE CASCADE ON UPDATE CASCADE,
    content    text        NOT NULL CHECK (content != ''),
    priority   integer     NOT NULL,
    created    timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
    modified   timestamptz CHECK (modified IS NULL OR modified > created),
    UNIQUE (user_email, priority) DEFERRABLE
);

CREATE TABLE IF NOT EXISTS keys (
    id      uuid        PRIMARY KEY,
    key     text        NOT NULL,
    expires timestamptz NOT NULL,
    created timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE INDEX IF NOT EXISTS keys_expires_idx ON keys (expires);