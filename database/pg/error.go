package pg

import (
	"github.com/lib/pq"
)

// ErrorCode shadows the pq.ErrorCode type.
type ErrorCode pq.ErrorCode

// Useful ErrorCode values.
const (
	ErrCodeUniqueViolation ErrorCode = "23505"
)
