package pg

import (
	"context"
	"database/sql"
	"fmt"
	"time"

	"github.com/pborman/uuid"
	"gitlab.com/feidtmb/notes/database"
	"gitlab.com/feidtmb/notes/domain"
)

func (db *DB) CreateNote(ctx context.Context, new database.NewNote) (string, error) {
	var id string
	err := db.db.QueryRowContext(ctx, `
		INSERT INTO notes (
			id, user_email, content, priority
		) VALUES (
			$1, $2, $3, COALESCE(
				(
					SELECT priority
					FROM notes
					WHERE user_email = $2
					ORDER BY priority DESC
					LIMIT 1
				),
				-1
			) + 1
		) RETURNING id;
	`, uuid.New(), new.UserEmail, new.Content).Scan(&id)
	return id, err
}

func (db *DB) GetNote(ctx context.Context, id string) (domain.Note, error) {
	var (
		n        domain.Note
		created  time.Time
		modified sql.NullTime
	)
	if err := db.db.QueryRowContext(ctx, `
		SELECT user_email, content, priority, created, modified
		FROM notes
		WHERE id = $1;
	`, id).Scan(&n.UserEmail, &n.Content, &n.Priority, &created, &modified); err != nil {
		if err == sql.ErrNoRows {
			return domain.Note{}, domain.Err{Kind: domain.ErrNotFound}
		}

		return domain.Note{}, err
	}

	n.ID = id
	n.Created = domain.NewTimestamp(created)
	if modified.Valid {
		n.Modified = domain.NewTimestamp(modified.Time)
	}

	return n, nil
}

func (db *DB) SearchNotes(ctx context.Context, userEmail string) ([]domain.Note, error) {
	rows, err := db.db.QueryContext(ctx, `
		SELECT id, user_email, content, priority, created, modified
		FROM notes
		WHERE user_email = $1;
	`, userEmail)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	ns := []domain.Note{} // Use a literal so we don't return nil if there are no results.
	for rows.Next() {
		var (
			n        domain.Note
			created  time.Time
			modified sql.NullTime
		)
		if err := rows.Scan(&n.ID, &n.UserEmail, &n.Content, &n.Priority, &created, &modified); err != nil {
			return nil, err
		}

		n.Created = domain.NewTimestamp(created)
		if modified.Valid {
			n.Modified = domain.NewTimestamp(modified.Time)
		}

		ns = append(ns, n)
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}

	return ns, nil
}

func (db *DB) ChangeNoteContent(ctx context.Context, changed database.ChangedNoteContent) error {
	_, err := db.db.ExecContext(ctx, `
		UPDATE notes SET
			content = $2,
			modified = CURRENT_TIMESTAMP
		WHERE id = $1;
	`, changed.ID, changed.Content)
	if err == sql.ErrNoRows {
		return domain.Err{Kind: domain.ErrNotFound}
	}
	return err
}

func (db *DB) ChangeNotePriority(ctx context.Context, changed database.ChangedNotePriority) error {
	switch changed.PriorityChange {
	case database.NotePriorityUp, database.NotePriorityDown:
		// Start a transaction, since we need to perform multiple operations.
		tx, err := db.db.BeginTx(ctx, nil)
		if err != nil {
			return err
		}
		// We can defer a rollback because if we commit first, then this ends up
		// as a no-op.
		defer tx.Rollback()

		// Get this note's associated user email and priority.
		var (
			userEmail string
			priority  int
		)
		if err := tx.QueryRowContext(ctx, `
			SELECT user_email, priority
			FROM notes
			WHERE id = $1;
		`, changed.ID).Scan(&userEmail, &priority); err != nil {
			return err
		}

		// Get the id and priority of the note we want to swap places with.
		var query string
		if changed.PriorityChange == database.NotePriorityUp {
			query = `
				SELECT id, priority
				FROM notes
				WHERE user_email = $1 AND priority > $2
				ORDER BY priority ASC
				LIMIT 1;
			`
		} else {
			query = `
				SELECT id, priority
				FROM notes
				WHERE user_email = $1 AND priority < $2
				ORDER BY priority DESC
				LIMIT 1;
			`
		}
		var (
			targetID       string
			targetPriority int
		)
		if err := tx.QueryRowContext(ctx, query, userEmail, priority).Scan(&targetID, &targetPriority); err != nil {
			if err == sql.ErrNoRows {
				// We're already at the highest or lowest priority.
				return nil
			}
			return err
		}

		// In order to swap the notes' priorities, we need to defer the priority
		// uniqueness check until this transaction is ready to commit.
		if _, err := tx.ExecContext(ctx, `
			SET CONSTRAINTS notes_user_email_priority_unique DEFERRED;
		`); err != nil {
			return err
		}

		// Swap the notes' priorities.
		if _, err := tx.ExecContext(ctx, `
			UPDATE notes SET
				priority = $2
			WHERE id = $1;
		`, targetID, priority); err != nil {
			return err
		}
		if _, err := tx.ExecContext(ctx, `
			UPDATE notes SET
				priority = $2
			WHERE id = $1;
		`, changed.ID, targetPriority); err != nil {
			return err
		}

		// Commit the transaction.
		return tx.Commit()
	case database.NotePriorityHighest:
		_, err := db.db.ExecContext(ctx, `
			UPDATE notes SET 
				priority = (
					SELECT priority + 1
					FROM notes
					WHERE user_email = (
						SELECT user_email
						FROM notes
						WHERE id = $1
					)
					ORDER BY priority DESC
					LIMIT 1
				)
			WHERE id = $1;
		`, changed.ID)
		if err == sql.ErrNoRows {
			return domain.Err{Kind: domain.ErrNotFound}
		}
		return err
	case database.NotePriorityLowest:
		_, err := db.db.ExecContext(ctx, `
			UPDATE notes SET 
				priority = (
					SELECT priority - 1
					FROM notes
					WHERE user_email = (
						SELECT user_email
						FROM notes
						WHERE id = $1
					)
					ORDER BY priority ASC
					LIMIT 1
				)
			WHERE id = $1;
		`, changed.ID)
		if err == sql.ErrNoRows {
			return domain.Err{Kind: domain.ErrNotFound}
		}
		return err
	default:
		return domain.Err{
			Kind:  domain.ErrInternal,
			Cause: fmt.Errorf("unrecognized note priority change: %q", changed.PriorityChange),
		}
	}
}

func (db *DB) DeleteNote(ctx context.Context, id string) error {
	_, err := db.db.ExecContext(ctx, `
		DELETE FROM notes
		WHERE id = $1;
	`, id)
	if err == sql.ErrNoRows {
		return domain.Err{Kind: domain.ErrNotFound}
	}
	return err
}
