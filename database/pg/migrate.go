package pg

import (
	"context"
	"io/ioutil"
	"path/filepath"
)

var migrations = filepath.Join("database", "pg", "migrations")

func (db *DB) Up(ctx context.Context) error {
	up, err := ioutil.ReadFile(filepath.Join(migrations, "up.sql"))
	if err != nil {
		return err
	}

	_, err = db.db.ExecContext(ctx, string(up))
	return err
}

func (db *DB) Down(ctx context.Context) error {
	down, err := ioutil.ReadFile(filepath.Join(migrations, "down.sql"))
	if err != nil {
		return err
	}

	_, err = db.db.ExecContext(ctx, string(down))
	return err
}
