#!/bin/bash

set -e
go test -cover -count=1 ./...
