// Package notes defines notes server.
package notes

import (
	"net/http"
	"net/url"

	"github.com/go-chi/chi"
	"gitlab.com/feidtmb/notes/database"
	"gitlab.com/feidtmb/notes/handler"
	"gitlab.com/feidtmb/notes/mail"
)

// Server is an HTTP server.
type Server struct {
	http.Handler
}

// New returns a new server instance.
func New(db database.DB, serverURL *url.URL, mailer mail.Mailer) *Server {
	router := chi.NewRouter()

	handler.RegisterRoutes(router, db, serverURL, mailer)

	return &Server{
		Handler: router,
	}
}
