package handler

import (
	"context"
	"time"

	"gitlab.com/feidtmb/lib/crypt"
	"gitlab.com/feidtmb/notes/database"
)

// NewUserToken returns a new token that embeds a user's email in its payload.
func NewUserToken(ctx context.Context, db database.DB, email string, expires time.Duration) (string, error) {
	// Get a fresh server key to use in a token.
	key, keyID, err := db.GetFreshKey(ctx)
	if err != nil {
		return "", err
	}

	// Create a signed token with the user email as the payload.
	token, err := crypt.Token(key, keyID, []byte(email), expires)
	if err != nil {
		return "", err
	}

	return token, nil
}

// ParseToken parses and verifies a token.
func ParseToken(ctx context.Context, db database.DB, token string) (payload string, issued time.Time, err error) {
	keyID, err := crypt.KeyIDFromToken(token)
	if err != nil {
		return "", time.Time{}, err
	}

	key, err := db.GetKey(ctx, keyID)
	if err != nil {
		return "", time.Time{}, err
	}

	payloadBytes, issued, err := crypt.ParseToken(key, token)
	return string(payloadBytes), issued, err
}
