package handler_test

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"gitlab.com/feidtmb/notes/handler"
)

func TestWithHTTPSOnly(t *testing.T) {
	mw := handler.WithHTTPSOnly()
	h := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		t.Fatal("Expected handler not to be called.")
	})

	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodGet, "http://example.com/login", nil)

	mw(h).ServeHTTP(w, r)

	res := w.Result()
	if res.StatusCode != http.StatusSeeOther {
		t.Errorf("Expected %s, got %s", http.StatusText(http.StatusSeeOther), res.Status)
	}

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		t.Fatal(err)
	}

	if !strings.Contains(string(body), `href="https://`) {
		t.Error("Expected body to contain HTTPS link")
	}
}
