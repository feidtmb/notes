package page

// NotFound returns the not found page.
func NotFound(loggedIn bool) *Page {
	data := defaultRootData
	data.Title += " | Not Found"
	data.LoggedIn = loggedIn

	return &Page{
		t:    templateMap["NotFound.html"],
		data: data,
	}
}
