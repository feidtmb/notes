package page

// RecoverPassword returns the recover password page.
func RecoverPassword(submittedEmail string) *Page {
	data := struct {
		RootData
		SubmittedEmail string
	}{
		RootData:       defaultRootData,
		SubmittedEmail: submittedEmail,
	}
	data.Title += " | Recover Password"

	return &Page{
		t:    templateMap["RecoverPassword.html"],
		data: data,
	}
}
