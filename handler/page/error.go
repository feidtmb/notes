package page

import (
	"gitlab.com/feidtmb/notes/domain"
)

// Error returns the error page.
func Error(err domain.Err, loggedIn bool) *Page {
	data := struct {
		RootData
		ErrorMessage string
	}{
		RootData:     defaultRootData,
		ErrorMessage: err.String(),
	}

	data.RootData.Title += " | Error"
	data.RootData.LoggedIn = loggedIn

	return &Page{
		t:    templateMap["Error.html"],
		data: data,
	}
}
