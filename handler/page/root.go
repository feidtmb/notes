package page

// RootData is the data for the root template, which all pages are expected to
// have as a parent.
type RootData struct {
	Author      string
	Description string
	Keywords    []string
	Title       string
	LoggedIn    bool
}

// The default root data used if none is provided.
var defaultRootData RootData = RootData{
	Author:      "Matthew Feidt",
	Description: "Brutally simple note taking.",
	Keywords:    []string{"note", "notes", "brutal"},
	Title:       "Brutal Note",
	LoggedIn:    false,
}
