package page

// Login returns the login page.
func Login() *Page {
	data := defaultRootData
	data.Title += " | Log In"

	return &Page{
		t:    templateMap["Login.html"],
		data: data,
	}
}
