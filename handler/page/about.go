package page

// About returns the about page.
func About(loggedIn bool) *Page {
	data := defaultRootData
	data.Title += " | About"
	data.LoggedIn = loggedIn

	return &Page{
		t:    templateMap["About.html"],
		data: data,
	}
}
