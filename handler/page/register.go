package page

// Register returns the register page.
func Register() *Page {
	data := defaultRootData
	data.Title += " | Register"

	return &Page{
		t:    templateMap["Register.html"],
		data: data,
	}
}
