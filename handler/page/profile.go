package page

import "gitlab.com/feidtmb/notes/domain"

// Profile returns the profile page.
func Profile(u domain.User, edit bool) *Page {
	data := struct {
		RootData
		User domain.User
		Edit bool
	}{
		RootData: defaultRootData,
		User:     u,
		Edit:     edit,
	}
	data.Title += " | Profile"

	return &Page{
		t:    templateMap["Profile.html"],
		data: data,
	}
}
