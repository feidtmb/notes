package page

import (
	"sort"

	"gitlab.com/feidtmb/notes/domain"
)

// Home returns the home page.
func Home(notes []domain.Note) *Page {
	sort.Sort(domain.NotesByPriority(notes))

	data := struct {
		RootData
		Notes []domain.Note
	}{
		RootData: defaultRootData,
		Notes:    notes,
	}

	data.RootData.LoggedIn = true

	return &Page{
		t:    templateMap["Home.html"],
		data: data,
	}
}
