package page

// ChangePassword returns the change password page.
func ChangePassword(email string, token string) *Page {
	data := struct {
		RootData
		Email string
		Token string
	}{
		RootData: defaultRootData,
		Email:    email,
		Token:    token,
	}
	data.Title += " | Change Password"

	return &Page{
		t:    templateMap["ChangePassword.html"],
		data: data,
	}
}
