// Package page defines HTML pages.
package page

import (
	"fmt"
	"html/template"
	"io"
	"io/ioutil"
	"log"
	"path/filepath"
	"runtime"
	"strings"
)

var templateMap map[string]*template.Template

func init() {
	// Figure out the path to our template files.
	//
	// Normally this will be relative to the root package (package notes), but
	// may change when running tests.
	_, thisFile, _, ok := runtime.Caller(0)
	if !ok {
		panic("couldn't determine absolute path to package page files")
	}

	templatesPath := filepath.Join(filepath.Dir(thisFile), "templates")

	// If we're in `/tmp` assume we're running on a cloud server, in which case
	// we shouldn't try to use an absolute path to the template files (likely to
	// be incorrect or break).
	if strings.HasPrefix(filepath.ToSlash(templatesPath), "/tmp/") {
		templatesPath = filepath.Join("handler", "page", "templates")
	}

	rootTemplate := template.Must(template.New("Root.html").Funcs(funcMap).ParseFiles(filepath.Join(templatesPath, "Root.html")))

	templateMap = make(map[string]*template.Template)
	files, err := ioutil.ReadDir(templatesPath)
	if err != nil {
		panic(fmt.Sprintf("failed to read template files: %v", err))
	}

	for _, f := range files {
		if !strings.HasSuffix(f.Name(), ".html") || f.Name() == "Root.html" {
			// f isn't a page template file
			continue
		}

		bs, err := ioutil.ReadFile(filepath.Join(templatesPath, f.Name()))
		if err != nil {
			panic(fmt.Sprintf("failed to read %s: %v", f.Name(), err))
		}

		// We're going to clone the root template, then parse the page template
		// file using the clone and store the result in our map.
		//
		// TODO (MBF): This is probably a nasty misuse of templates that should
		// be improved after I take time to better understand the templates
		// package.
		rootClone := template.Must(rootTemplate.Clone())
		templateMap[f.Name()] = template.Must(rootClone.Parse(string(bs)))
	}
}

// The function map provided to templates.
var funcMap = template.FuncMap{
	"join": strings.Join,
}

// Page is an HTML page.
type Page struct {
	t    *template.Template
	data interface{}
}

// Render writes the page to wr.
func (p Page) Render(wr io.Writer) {
	if err := p.t.Execute(wr, p.data); err != nil {
		log.Printf("Failed to render %q page: %v", p.t.Name(), err)
	}
}
