package page

import "gitlab.com/feidtmb/notes/domain"

// Note returns the note page.
func Note(note domain.Note, delete bool) *Page {
	data := struct {
		RootData
		Note   domain.Note
		Delete bool
	}{
		RootData: defaultRootData,
		Note:     note,
		Delete:   delete,
	}

	data.RootData.LoggedIn = true
	data.Title += " | " + note.ID
	if note.ID == "" {
		data.Title += " | New"
	}

	return &Page{
		t:    templateMap["Note.html"],
		data: data,
	}
}
