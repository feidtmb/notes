package handler

import (
	"errors"
	"fmt"
	"net/http"
	"net/url"
	"strings"
	"time"

	"gitlab.com/feidtmb/lib/crypt"
	"gitlab.com/feidtmb/notes/database"
	"gitlab.com/feidtmb/notes/domain"
	"gitlab.com/feidtmb/notes/mail"
)

// Data size units.
const (
	Kilobyte = 1000
	Megabyte = Kilobyte * 1000
)

// Authenticate handles a request to authenticate a user.
func Authenticate(db database.DB) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		email := strings.TrimSpace(r.FormValue("email"))
		password := r.FormValue("password")
		persist := r.FormValue("persist") != ""

		var vs []string // validation messages
		if email == "" {
			vs = append(vs, "email is required")
		}
		if password == "" {
			vs = append(vs, "password is required")
		}
		if len(vs) > 0 {
			Error(w, r, domain.Err{
				Kind:    domain.ErrRequest,
				Message: fmt.Sprintf("Invalid request: %s.", strings.Join(vs, ", ")),
			})
			return
		}

		// Define one failed authentication error to avoid leaking any
		// information.
		errAuthFailed := domain.Err{
			Kind:    domain.ErrRequest,
			Message: "Incorrect email or password.",
		}

		u, err := db.GetUser(r.Context(), email)
		if err != nil {
			errAuthFailed.Cause = err

			// Handle email matching no user.
			if e, ok := err.(domain.Err); ok {
				if e.Kind == domain.ErrNotFound {
					err = errAuthFailed
				}
			}

			Error(w, r, err)
			return
		}

		if !crypt.MatchesOneWayHash([]byte(password), []byte(u.PasswordHash)) {
			Error(w, r, errAuthFailed)
			return
		}

		c, err := NewCookie(r.Context(), db, email, persist)
		if err != nil {
			Error(w, r, err)
			return
		}

		http.SetCookie(w, c)
		http.Redirect(w, r, "/", http.StatusSeeOther)
	})
}

// Deauthenticate handles a request to deauthenticate a user.
func Deauthenticate() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// TODO: Support logging out from all devices.
		c := NewEmptyCookie()
		http.SetCookie(w, c)
		http.Redirect(w, r, "/login", http.StatusSeeOther)
	})
}

// MaxNoteContentSize is our limit on the size of a note's content.
const MaxNoteContentSize = Megabyte * 10

// CreateNote handles a request to create a new note.
func CreateNote(db database.DB) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		content := strings.TrimSpace(r.FormValue("content"))

		var vs []string // validation messages
		if content == "" {
			vs = append(vs, "note can't be empty")
		}
		if len(content) > MaxNoteContentSize {
			vs = append(vs, "note is too large")
		}
		if len(vs) > 0 {
			Error(w, r, domain.Err{
				Kind:    domain.ErrRequest,
				Message: fmt.Sprintf("Invalid request: %s.", strings.Join(vs, ", ")),
			})
			return
		}

		ctx := r.Context()
		_, err := db.CreateNote(ctx, database.NewNote{
			UserEmail: UserFromContext(ctx).Email,
			Content:   content,
		})
		if err != nil {
			Error(w, r, err)
			return
		}

		http.Redirect(w, r, "/", http.StatusSeeOther)
	})
}

// ChangeNoteContent handles a request to change a note's content.
func ChangeNoteContent(db database.DB) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		id := r.FormValue("id")
		content := strings.TrimSpace(r.FormValue("content"))

		var vs []string // validation messages
		if id == "" {
			vs = append(vs, "note id is missing")
		}
		if content == "" {
			vs = append(vs, "note can't be empty")
		}
		if len(content) > MaxNoteContentSize {
			vs = append(vs, "note is too large")
		}
		if len(vs) > 0 {
			Error(w, r, domain.Err{
				Kind:    domain.ErrRequest,
				Message: fmt.Sprintf("Invalid request: %s.", strings.Join(vs, ", ")),
			})
			return
		}

		// Ensure user owns note.
		ctx := r.Context()
		note, err := db.GetNote(ctx, id)
		if err != nil {
			Error(w, r, err)
			return
		}
		if note.UserEmail != UserFromContext(ctx).Email {
			Error(w, r, domain.Err{
				Kind:  domain.ErrNotFound,
				Cause: errors.New("use attempting to modify other's note"),
			})
			return
		}

		if err := db.ChangeNoteContent(ctx, database.ChangedNoteContent{
			ID:      id,
			Content: content,
		}); err != nil {
			Error(w, r, err)
			return
		}

		http.Redirect(w, r, "/", http.StatusSeeOther)
	})
}

// ChangeNotePriority handles a request to change a note's sort priority.
func ChangeNotePriority(db database.DB) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		id := r.FormValue("id")
		change := database.NotePriorityChange(r.FormValue("priority_change"))

		var vs []string // validation messages
		if id == "" {
			vs = append(vs, "note id is missing")
		}
		switch change {
		case database.NotePriorityUp, database.NotePriorityDown, database.NotePriorityHighest, database.NotePriorityLowest:
			// All good.
		case "":
			vs = append(vs, "note priority change is missing")
		default:
			vs = append(vs, "note priority change is unrecognized")
		}
		if len(vs) > 0 {
			Error(w, r, domain.Err{
				Kind:    domain.ErrRequest,
				Message: fmt.Sprintf("Invalid request: %s.", strings.Join(vs, ", ")),
			})
			return
		}

		// Ensure user owns note.
		ctx := r.Context()
		note, err := db.GetNote(ctx, id)
		if err != nil {
			Error(w, r, err)
			return
		}
		if note.UserEmail != UserFromContext(ctx).Email {
			Error(w, r, domain.Err{
				Kind:  domain.ErrNotFound,
				Cause: errors.New("use attempting to modify other's note"),
			})
			return
		}

		if err := db.ChangeNotePriority(ctx, database.ChangedNotePriority{
			ID:             id,
			PriorityChange: change,
		}); err != nil {
			Error(w, r, err)
			return
		}

		http.Redirect(w, r, "/", http.StatusSeeOther)
	})
}

// DeleteNote handles a request to delete a note.
func DeleteNote(db database.DB) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// Ensure user owns note.
		id := r.FormValue("id")
		ctx := r.Context()
		note, err := db.GetNote(ctx, id)
		if err != nil {
			Error(w, r, err)
			return
		}
		if note.UserEmail != UserFromContext(ctx).Email {
			Error(w, r, domain.Err{
				Kind:  domain.ErrNotFound,
				Cause: errors.New("use attempting to delete other's note"),
			})
			return
		}

		if err := db.DeleteNote(ctx, id); err != nil {
			Error(w, r, err)
			return
		}

		http.Redirect(w, r, "/", http.StatusSeeOther)
	})
}

// Limits on saved user account values' sizes.
const (
	// MaxUserEmailSize is our limit on the size of a user's email address.
	MaxUserEmailSize = Kilobyte
	// MaxUserPasswordSize is our limit on the size of a user's password.
	MaxUserPasswordSize = Kilobyte * 10
)

// CreateUser handles a request to create a new user.
func CreateUser(db database.DB) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		email := strings.TrimSpace(r.FormValue("email"))
		password := r.FormValue("password")

		var vs []string // validation messages
		if email == "" {
			vs = append(vs, "email is required")
		}
		if len(email) > MaxUserEmailSize {
			vs = append(vs, "email is too long")
		}
		if password == "" {
			vs = append(vs, "password is required")
		}
		if len(password) > MaxUserPasswordSize {
			vs = append(vs, "password is too long")
		}
		if len(vs) > 0 {
			Error(w, r, domain.Err{
				Kind:    domain.ErrRequest,
				Message: fmt.Sprintf("Invalid request: %s.", strings.Join(vs, ", ")),
			})
			return
		}

		passwordHash, err := crypt.OneWayHash([]byte(password))
		if err != nil {
			Error(w, r, err)
			return
		}

		if err := db.CreateUser(r.Context(), database.NewUser{
			Email:        email,
			PasswordHash: string(passwordHash),
		}); err != nil {
			Error(w, r, err)
			return
		}

		c, err := NewCookie(r.Context(), db, email, false)
		if err != nil {
			Error(w, r, err)
			return
		}

		http.SetCookie(w, c)
		http.Redirect(w, r, "/", http.StatusSeeOther)
	})
}

// ChangeUserEmail handles a request to change a user's email.
func ChangeUserEmail(db database.DB) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		email := strings.TrimSpace(r.FormValue("email"))

		var vs []string // validation messages
		if email == "" {
			vs = append(vs, "email is required")
		}
		if len(email) > MaxUserEmailSize {
			vs = append(vs, "email is too long")
		}
		if len(vs) > 0 {
			Error(w, r, domain.Err{
				Kind:    domain.ErrRequest,
				Message: fmt.Sprintf("Invalid request: %s.", strings.Join(vs, ", ")),
			})
			return
		}

		ctx := r.Context()
		if err := db.ChangeUserEmail(ctx, database.ChangedUserEmail{
			Email:    UserFromContext(ctx).Email,
			NewEmail: email,
		}); err != nil {
			Error(w, r, err)
			return
		}

		// Create a new cookie for the user, but be conservative and don't
		// persist it in case it wasn't persisted previously.
		c, err := NewCookie(ctx, db, email, false)
		if err != nil {
			Error(w, r, err)
			return
		}

		http.SetCookie(w, c)
		http.Redirect(w, r, "/profile", http.StatusSeeOther)
	})
}

// ChangeUserPassword handles a request to change a user's password.
func ChangeUserPassword(db database.DB) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		password := r.FormValue("password")
		confirmPassword := r.FormValue("confirm_password")

		var vs []string // validation messages
		if password != confirmPassword {
			vs = append(vs, "passwords must match")
		}
		if password == "" {
			vs = append(vs, "password is required")
		}
		if len(password) > MaxUserPasswordSize {
			vs = append(vs, "password is too long")
		}
		if len(vs) > 0 {
			Error(w, r, domain.Err{
				Kind:    domain.ErrRequest,
				Message: fmt.Sprintf("Invalid request: %s.", strings.Join(vs, ", ")),
			})
			return
		}

		passwordHash, err := crypt.OneWayHash([]byte(password))
		if err != nil {
			Error(w, r, err)
			return
		}

		// If we have a logged-in user, we'll use that and ignore any token that
		// may be present.
		ctx := r.Context()
		email := UserFromContext(ctx).Email
		if email == "" {
			// We don't have a logged-in user, so we'll need a valid token.
			token := r.FormValue("token")

			payload, issued, err := ParseToken(ctx, db, token)
			if err != nil {
				// Token is invalid or expired.
				Error(w, r, err)
				return
			}

			email = string(payload)
			u, err := db.GetUser(ctx, email)
			if err != nil {
				Error(w, r, err)
				return
			}

			if issued.Before(u.Reauthenticate) {
				// Token was revoked.
				Error(w, r, domain.Err{
					Kind:    domain.ErrRequest,
					Message: "The provided change password token was revoked.",
				})
				return
			}
		}

		// We either have a logged-in user, or a valid token (from which we
		// extracted a user's email). In either case, we're ready to make the
		// change in the database.
		if err := db.ChangeUserPassword(ctx, database.ChangedUserPassword{
			Email:        email,
			PasswordHash: string(passwordHash),
		}); err != nil {
			Error(w, r, err)
			return
		}

		// The password change should have revoked any issued credentials, so
		// we'll redirect the user to the log in page. We could re-issue
		// credentials automatically, but we won't because we want the user to
		// be confident their password was changed and also encourage them to
		// use it at least once to ensure they have it recorded properly.
		http.Redirect(w, r, "/login", http.StatusSeeOther)
	})
}

// RecoverUserPassword handles a request to recover a user's password.
func RecoverUserPassword(db database.DB, serverURL *url.URL, mailer mail.Mailer) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		email := strings.TrimSpace(r.FormValue("email"))

		var vs []string // validation messages
		if email == "" {
			vs = append(vs, "email is required")
		}
		if len(vs) > 0 {
			Error(w, r, domain.Err{
				Kind:    domain.ErrRequest,
				Message: fmt.Sprintf("Invalid request: %s.", strings.Join(vs, ", ")),
			})
			return
		}

		redirectQuery := url.Values{}
		redirectQuery.Set("email", email)
		redirectPath := fmt.Sprintf("/recover-password?%s", redirectQuery.Encode())

		ctx := r.Context()
		u, err := db.GetUser(ctx, email)
		if err != nil {
			// If this is the not found error, we don't want to leak that
			// information to the requestor.
			if e, ok := err.(domain.Err); ok {
				if e.Kind == domain.ErrNotFound {
					// Just redirect the user without relaying this error.
					http.Redirect(w, r, redirectPath, http.StatusSeeOther)
					return
				}
			}

			// Error isn't related to the user not existing.
			Error(w, r, err)
			return
		}

		// Generate a token for use in an emailed link.
		token, err := NewUserToken(ctx, db, u.Email, time.Hour*24*3)
		if err != nil {
			Error(w, r, err)
			return
		}

		// Assemble the recover password link.
		recoverQuery := url.Values{}
		recoverQuery.Set("token", token)
		recoverLink := fmt.Sprintf("%s/change-password?%s", serverURL, recoverQuery.Encode())

		// Email the link to the user.
		// TODO: Improve.
		if err := mailer.Send(ctx, mail.Message{
			From:    fmt.Sprintf("noreply@%s", serverURL.Hostname()),
			Tos:     []string{email},
			Subject: "Recover Password Request",
			Body:    fmt.Sprintf("Use the following link to recover your password: %s", recoverLink),
		}); err != nil {
			Error(w, r, err)
			return
		}

		// Do the same thing we'd do if no account existed for the entered email
		// address.
		http.Redirect(w, r, redirectPath, http.StatusSeeOther)
	})
}
