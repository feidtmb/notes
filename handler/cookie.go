package handler

import (
	"context"
	"errors"
	"net/http"
	"time"

	"gitlab.com/feidtmb/notes/database"
)

// Cookie name.
const cookieName = "user"

// NewCookie creates a secure, signed, expiring HTTP cookie containing an email
// address.
func NewCookie(ctx context.Context, db database.DB, email string, persist bool) (*http.Cookie, error) {
	// Set when the token expires based on whether or not we're persisting the
	// cookie.
	expires := 24 * time.Hour // One day.
	if persist {
		expires = 365 * 24 * time.Hour // One year (or more realistically, until the key expires).
	}

	// Create a user token for the cookie's payload.
	token, err := NewUserToken(ctx, db, email, expires)
	if err != nil {
		return nil, err
	}

	// Set the cookie's expiration time.
	// If persisting, it will match the token's (roughly). If not persisting, it
	// will be nil to indicate it should be a session cookie that's deleted when
	// the session ends. Note that the key's expiration time will override this
	// time if it is sooner.
	var cookieExpires time.Time
	if persist {
		cookieExpires = time.Now().Add(expires)
	}

	return &http.Cookie{
		Name:     cookieName,
		Value:    token,
		Path:     "/",
		Expires:  cookieExpires,
		HttpOnly: true,
		SameSite: http.SameSiteStrictMode,
	}, nil
}

// NewEmptyCookie returns an HTTP cookie that may be used to overwrite a
// previously issued cookie with one containing no information.
func NewEmptyCookie() *http.Cookie {
	return &http.Cookie{
		Name:     cookieName,
		Value:    "",
		Path:     "/",
		HttpOnly: true,
		SameSite: http.SameSiteStrictMode,
	}
}

// FromCookie extracts an email address and issued time from a cookie created
// with NewCookie. If the cookie has been tampered with or is expired, an error
// will be returned. The issued time may be used to support expiring cookies
// server-side.
func FromCookie(db database.DB, r *http.Request) (string, time.Time, error) {
	c, err := r.Cookie(cookieName)
	if err != nil {
		return "", time.Time{}, err
	}

	if c.Value == "" {
		// Empty cookie, likely meaning a valid one was revoked.
		return "", time.Time{}, errors.New("empty cookie")
	}

	return ParseToken(r.Context(), db, c.Value)
}
