package handler

import (
	"net/http"
	"net/url"

	"github.com/go-chi/chi"
	"gitlab.com/feidtmb/notes/database"
	"gitlab.com/feidtmb/notes/mail"
)

// RegisterRoutes registers middleware and endpoints onto the provided router.
func RegisterRoutes(router chi.Router, db database.DB, serverURL *url.URL, mailer mail.Mailer) {
	if serverURL.Scheme == "https" {
		router.Use(WithHTTPSOnly())
	}
	router.Use(WithAuthenticated(db))

	// Unrestricted routes.
	router.NotFound(NotFound().ServeHTTP)
	router.Method(http.MethodGet, "/about", About())
	router.Method(http.MethodGet, "/change-password", ChangePassword(db))
	router.Method(http.MethodPost, "/forms/change-user-password", ChangeUserPassword(db))

	// Unauthenticated only routes.
	router.With(WithRedirectAuthenticated()).Method(http.MethodGet, "/login", Login())
	router.With(WithRedirectAuthenticated()).Method(http.MethodGet, "/register", Register())
	router.With(WithRedirectAuthenticated()).Method(http.MethodGet, "/recover-password", RecoverPassword())
	router.With(WithRedirectAuthenticated()).Method(http.MethodPost, "/forms/login", Authenticate(db))
	router.With(WithRedirectAuthenticated()).Method(http.MethodPost, "/forms/register", CreateUser(db))
	router.With(WithRedirectAuthenticated()).Method(http.MethodPost, "/forms/recover-user-password", RecoverUserPassword(db, serverURL, mailer))

	// Authenticated only routes.
	router.With(WithRedirectUnauthenticated()).Method(http.MethodGet, "/", Home(db))
	router.With(WithRedirectUnauthenticated()).Method(http.MethodGet, "/logout", Deauthenticate())
	router.With(WithRedirectUnauthenticated()).Method(http.MethodGet, "/note", Note(db))
	router.With(WithRedirectUnauthenticated()).Method(http.MethodGet, "/profile", Profile())
	router.With(WithRedirectUnauthenticated()).Method(http.MethodPost, "/forms/create-note", CreateNote(db))
	router.With(WithRedirectUnauthenticated()).Method(http.MethodPost, "/forms/change-note-content", ChangeNoteContent(db))
	router.With(WithRedirectUnauthenticated()).Method(http.MethodPost, "/forms/change-note-priority", ChangeNotePriority(db))
	router.With(WithRedirectUnauthenticated()).Method(http.MethodPost, "/forms/delete-note", DeleteNote(db))
	router.With(WithRedirectUnauthenticated()).Method(http.MethodPost, "/forms/change-user-email", ChangeUserEmail(db))
}
