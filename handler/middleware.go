package handler

import (
	"log"
	"net/http"

	"gitlab.com/feidtmb/notes/database"
	"gitlab.com/feidtmb/notes/domain"
)

// Middleware is a function that takes an HTTP handler and returns an HTTP
// handler.
type Middleware func(next http.Handler) http.Handler

// WithAuthenticated adds an authenticated user to the request context if valid
// credentials were provided with the request.
func WithAuthenticated(db database.DB) Middleware {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			email, issued, err := FromCookie(db, r)
			if err != nil {
				// Omitted or invalid cookie.
				next.ServeHTTP(w, r)
				return
			}

			ctx := r.Context()

			u, err := db.GetUser(ctx, email)
			if err != nil {
				log.Printf("Error getting authenticated user information: %v", err)
				next.ServeHTTP(w, r)
				return
			}

			if issued.Before(u.Reauthenticate) {
				// Cookie was revoked.
				next.ServeHTTP(w, r)
				return
			}

			ctx = ContextWithUser(ctx, u)
			r = r.WithContext(ctx)

			next.ServeHTTP(w, r)
		})
	}
}

// WithRedirectUnauthenticated is middleware that redirects unauthenticated
// users to the login page.
func WithRedirectUnauthenticated() Middleware {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if u := UserFromContext(r.Context()); u.Email == "" {
				http.Redirect(w, r, "/login", http.StatusSeeOther)
				return
			}

			next.ServeHTTP(w, r)
		})
	}
}

// WithRedirectAuthenticated is middleware that redirects authenticated users
// to the home page.
func WithRedirectAuthenticated() Middleware {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if u := UserFromContext(r.Context()); u.Email != "" {
				http.Redirect(w, r, "/", http.StatusSeeOther)
				return
			}

			next.ServeHTTP(w, r)
		})
	}
}

// WithHTTPSOnly redirects HTTP requests to HTTPS, unless running locally.
func WithHTTPSOnly() Middleware {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			// Redirect HTTP to HTTPS if not running locally.
			if r.URL.Scheme == "http" && r.URL.Hostname() != "localhost" {
				// Redirect to HTTPS.
				r.URL.Scheme = "https"
				http.Redirect(w, r, r.URL.String(), http.StatusSeeOther)
				return
			}

			// Handle cases where the scheme was stripped by a proxy (happens on
			// Heroku for example).
			if r.URL.Scheme == "" && r.Header.Get("X-Forwarded-Proto") == "http" {
				// Not sure how to redirect wihtout a hardcoded URL, so for now
				// we'll just forbid the request.
				// TODO: Figure out a better solution.
				Error(w, r, domain.Err{
					Kind:    domain.ErrRequest,
					Message: "Please navigate to the https:// version of the site, http:// is not secure.",
				})
				return
			}

			next.ServeHTTP(w, r)
		})
	}
}
