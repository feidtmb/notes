// Package handler defines HTTP handlers for the website.
package handler

import (
	"net/http"
	"strconv"

	"gitlab.com/feidtmb/notes/database"
	"gitlab.com/feidtmb/notes/domain"
	"gitlab.com/feidtmb/notes/handler/page"
)

// About handles requests for the about page.
func About() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		u := UserFromContext(r.Context())
		page.About(u.Email != "").Render(w)
	})
}

// Home handles requests for the home page.
func Home(db database.DB) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		notes, err := db.SearchNotes(ctx, UserFromContext(ctx).Email)
		if err != nil {
			Error(w, r, err)
			return
		}

		page.Home(notes).Render(w)
	})
}

// Login handles requests for the login page.
func Login() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		page.Login().Render(w)
	})
}

// RecoverPassword handles requests for the recover password page.
func RecoverPassword() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		query := r.URL.Query()
		page.RecoverPassword(query.Get("email")).Render(w)
	})
}

// Note handles requests for the note page.
//
// If id exists in the request URL query string, then the page will contain
// functionality for either editing or deleting (if delete exists in the URL
// query string and is truthy) the identified note. Otherwise it will contain
// functionality to create a new note.
func Note(db database.DB) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		query := r.URL.Query()

		if id := query.Get("id"); id != "" {
			ctx := r.Context()
			note, err := db.GetNote(ctx, id)
			if err != nil {
				Error(w, r, err)
				return
			}

			delete, _ := strconv.ParseBool(query.Get("delete"))

			page.Note(note, delete).Render(w)
			return
		}

		page.Note(domain.Note{}, false).Render(w)
	})
}

// Register handles requests for the register page.
func Register() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		page.Register().Render(w)
	})
}

// Profile handles requests for the profile page.
func Profile() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		u := UserFromContext(r.Context())
		query := r.URL.Query()
		edit, _ := strconv.ParseBool(query.Get("edit"))
		page.Profile(u, edit).Render(w)
	})
}

// ChangePassword handles requests for the change password page.
func ChangePassword(db database.DB) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// If we have a logged-in user, then we'll just render the page for them
		// and ignore any token that may be present.
		ctx := r.Context()
		u := UserFromContext(ctx)
		if u.Email != "" {
			page.ChangePassword(u.Email, "").Render(w)
			return
		}

		query := r.URL.Query()
		token := query.Get("token")

		if token == "" {
			// We have no form of authentication.
			http.Redirect(w, r, "/login", http.StatusSeeOther)
			return
		}

		payload, issued, err := ParseToken(ctx, db, token)
		if err != nil {
			// Token is invalid or expired.
			Error(w, r, err)
			return
		}

		// We could stop at this point and just render the page, but we'll
		// proceed to ensure the token is not revoked in order to avoid
		// potentially wasting the user's time. This check will necessarily be
		// redone when we process the change password form submission.

		email := string(payload)
		u, err = db.GetUser(ctx, email)
		if err != nil {
			Error(w, r, err)
			return
		}

		if issued.Before(u.Reauthenticate) {
			// Token was revoked.
			Error(w, r, domain.Err{
				Kind:    domain.ErrRequest,
				Message: "The provided change password token was revoked.",
			})
			return
		}

		page.ChangePassword(email, token).Render(w)
	})
}

// NotFound handles requests for pages that don't exist.
//
// If an error page is desired, use Error instead.
func NotFound() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		u := UserFromContext(r.Context())
		w.WriteHeader(http.StatusNotFound)
		page.NotFound(u.Email != "").Render(w)
	})
}
