package handler

import (
	"context"

	"gitlab.com/feidtmb/notes/domain"
)

type contextKey int

// Keys used to store different values in context.
const (
	userKey contextKey = iota
)

// ContextWithUser returns a copy of context with the user added.
func ContextWithUser(ctx context.Context, u domain.User) context.Context {
	return context.WithValue(ctx, userKey, u)
}

// UserFromContext extracts a user from context.
func UserFromContext(ctx context.Context) domain.User {
	if u, ok := ctx.Value(userKey).(domain.User); ok {
		return u
	}
	return domain.User{}
}
