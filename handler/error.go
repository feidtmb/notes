package handler

import (
	"log"
	"net/http"

	"gitlab.com/feidtmb/notes/domain"
	"gitlab.com/feidtmb/notes/handler/page"
)

// Error responds to a request with an error page.
func Error(w http.ResponseWriter, r *http.Request, err error) {
	var e domain.Err

	if de, ok := err.(domain.Err); ok {
		e = de
	} else {
		e.Cause = err
	}

	if e.HTTPStatusCode() >= 500 {
		log.Printf("%d: %v (%v)", e.HTTPStatusCode(), e.Error(), e.Kind)
	}

	u := UserFromContext(r.Context())

	w.WriteHeader(e.HTTPStatusCode())
	page.Error(e, u.Email != "").Render(w)
}
