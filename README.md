# Notes

For taking notes.

## Hosting

Hosted on Heroku.

- Deploy: `git push heroku [branch]`
- Logs: `heroku logs --tail`
- PostgreSQL DB: `heroku pg:psql`

## Code Organization

**`/`**

Defines the `Server` type, which is the main entry point to the application.

**`/cmd`**

Contains executable packages, including a package to actually serve the application.

**`/database`**

Defines the interface for a database, and contains packages with implementations.

**`/domain`**

Domain types shared throughout the application's packages.

**`/handler`**

HTTP request handlers, and contains a package for rendering HTML pages.

**`/mail`**

Defines the interface for sending email, and contains packages with implementations.
