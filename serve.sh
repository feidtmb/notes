#!/bin/bash

set -e
go build -o ./bin/serve ./cmd/serve
ENV=local URL="http://localhost:8080" ./bin/serve
