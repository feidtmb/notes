// Package mail defines an interface for sending email.
package mail

import (
	"context"
	"fmt"
	"strings"

	"gitlab.com/feidtmb/notes/domain"
)

// Mailer is the interface for sending email.
type Mailer interface {
	Send(context.Context, Message) error
}

// Message is an email.
type Message struct {
	From    string
	Tos     []string
	CCs     []string
	BCCs    []string
	Subject string
	Body    string
}

// Validate ensures a Message contains necessary information.
//
// This should not replace validation of information from a request, and
// therefore returns an error Kind of ErrInternal.
func (m Message) Validate() error {
	var vs []string // validation messages
	if m.From == "" {
		vs = append(vs, "from address is required")
	}
	if len(m.Tos) == 0 {
		vs = append(vs, "at least one to address is required")
	}
	if m.Subject == "" {
		vs = append(vs, "subject is required")
	}
	if m.Body == "" {
		vs = append(vs, "body is required")
	}
	if len(vs) > 0 {
		return domain.Err{
			Kind:  domain.ErrInternal,
			Cause: fmt.Errorf("invalid message: %s", strings.Join(vs, ", ")),
		}
	}
	return nil
}
