// Package iomail sends email to stdout. Meant for testing only.
package iomail

import (
	"context"
	"log"
	"strings"

	"gitlab.com/feidtmb/notes/mail"
)

// Mailer implements mail.Mailer.
type Mailer struct{}

// New returns a new Mailer.
func New() Mailer {
	return Mailer{}
}

// Send logs msg to stdout.
func (Mailer) Send(ctx context.Context, msg mail.Message) error {
	if err := msg.Validate(); err != nil {
		return err
	}

	log.Println("New message:")
	log.Printf("\tFrom: %v", msg.From)
	log.Printf("\tTo: %v", strings.Join(msg.Tos, ", "))
	log.Printf("\tCC: %v", strings.Join(msg.CCs, ", "))
	log.Printf("\tBCC: %v", strings.Join(msg.BCCs, ", "))
	log.Printf("\tSubject: %v", msg.Subject)
	log.Printf("\tBody:\n%v", msg.Body)

	return nil
}
