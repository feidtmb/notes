package mailgun

import (
	"context"
	"os"
	"testing"

	"gitlab.com/feidtmb/notes/mail"
)

func TestSend(t *testing.T) {
	domain := os.Getenv("MAILGUN_DOMAIN")
	key := os.Getenv("MAILGUN_KEY")

	if domain == "" || key == "" {
		t.Skip("Mailgun domain and key are required for test send.")
	}

	mg := New(domain, key)
	if err := mg.Send(context.Background(), mail.Message{
		From:    "noreply@" + domain,
		Tos:     []string{"test@" + domain},
		Subject: "Test",
		Body:    "This is a test.",
	}); err != nil {
		t.Fatal(err)
	}
}
