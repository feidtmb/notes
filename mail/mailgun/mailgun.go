// Package mailgun uses mailgun to send email.
package mailgun

import (
	"context"
	"fmt"

	"github.com/mailgun/mailgun-go/v4"
	"gitlab.com/feidtmb/notes/domain"
	"gitlab.com/feidtmb/notes/mail"
)

// Mailer implements mail.Mailer.
type Mailer struct {
	mg *mailgun.MailgunImpl
}

// New returns a new Mailer.
func New(domain string, key string) Mailer {
	mg := mailgun.NewMailgun(domain, key)
	return Mailer{mg}
}

// Send sends an email using mailgun.
func (m Mailer) Send(ctx context.Context, msg mail.Message) error {
	if err := msg.Validate(); err != nil {
		return err
	}

	mgMsg := m.mg.NewMessage(msg.From, msg.Subject, msg.Body, msg.Tos...)
	for _, cc := range msg.CCs {
		mgMsg.AddCC(cc)
	}
	for _, bcc := range msg.BCCs {
		mgMsg.AddBCC(bcc)
	}

	if _, _, err := m.mg.Send(ctx, mgMsg); err != nil {
		return domain.Err{
			Kind:  domain.ErrInternal,
			Cause: fmt.Errorf("error sending email via mailgun: %w", err),
		}
	}

	return nil
}
