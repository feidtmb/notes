package notes_test

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strings"
	"testing"

	"gitlab.com/feidtmb/notes"
	"gitlab.com/feidtmb/notes/database/mem"
	"gitlab.com/feidtmb/notes/mail/iomail"
)

func TestNotes(t *testing.T) {
	db, err := mem.New(nil)
	if err != nil {
		t.Fatal(err)
	}

	serverURL, _ := url.Parse("http://localhost:8080")
	s := notes.New(db, serverURL, iomail.New())

	cases := []struct {
		Name              string
		Request           *http.Request
		WantStatus        int
		WantBodyToContain []string
	}{
		{
			Name:              "Login page display",
			Request:           httptest.NewRequest(http.MethodGet, "/login", nil),
			WantStatus:        http.StatusOK,
			WantBodyToContain: []string{"log in"},
		},
		{
			Name:              "About page display",
			Request:           httptest.NewRequest(http.MethodGet, "/about", nil),
			WantStatus:        http.StatusOK,
			WantBodyToContain: []string{"about"},
		},
		{
			Name:              "Not found page display",
			Request:           httptest.NewRequest(http.MethodGet, "/non-existant", nil),
			WantStatus:        http.StatusNotFound,
			WantBodyToContain: []string{"not found"},
		},
	}

	for _, c := range cases {
		t.Run(c.Name, func(t *testing.T) {
			w := httptest.NewRecorder()
			s.ServeHTTP(w, c.Request)

			response := w.Result()
			if response.StatusCode != c.WantStatus {
				t.Errorf("Expected status code %d, got %d", c.WantStatus, response.StatusCode)
			}

			responseBody, err := ioutil.ReadAll(response.Body)
			if err != nil {
				t.Fatal(err)
			}

			t.Log(string(responseBody))

			for _, want := range c.WantBodyToContain {
				if !strings.Contains(strings.ToLower(string(responseBody)), want) {
					t.Errorf("Expected body to contain %q", want)
				}
			}
		})
	}
}
