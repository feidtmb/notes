package domain

import "time"

// User is a user account.
type User struct {
	// Email is the user's email, used to send account-related communications.
	// It identifies the user and must be unique.
	Email string `json:"email"`

	// PasswordHash is the hash of the user's password.
	PasswordHash string `json:"-"`

	// Reauthenticate is the last time credentials issued to this user were
	// revoked. This means any credential issued to this user before this time
	// should be rejected.
	Reauthenticate time.Time `json:"-"`

	// Created is the time the user account was originally created.
	Created Timestamp `json:"created"`
}
