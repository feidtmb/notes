package domain

// Note is a user-created note.
type Note struct {
	// ID uniquely identifies the note, and must be safe for use in a URL.
	ID string `json:"id"`

	// UserEmail is the email of the user that owns the note.
	UserEmail string `json:"-"`

	// Content is the body of the note.
	Content string `json:"content"`

	// Priority is used to adjust sort priority relative to other notes.
	//
	// The value of this field is volatile, and must only be considered relative
	// to other notes' Priority values.
	Priority int `json:"-"`

	// Created is the time the note was originally created.
	Created Timestamp `json:"created"`

	// Modified is the time the note was last changed.
	Modified Timestamp `json:"modified,omitempty"`
}

// NotesByPriority is a slice of Note that implements sort.Interface to sort by
// highest to lowest Priority.
type NotesByPriority []Note

func (ns NotesByPriority) Len() int {
	return len(ns)
}

func (ns NotesByPriority) Swap(i, j int) {
	ns[i], ns[j] = ns[j], ns[i]
}

func (ns NotesByPriority) Less(i, j int) bool {
	return ns[i].Priority > ns[j].Priority
}

// NotesByCreated is a slice of Note that implements sort.Interface to sort
// newest to oldest based on the Created timestamp.
type NotesByCreated []Note

func (ns NotesByCreated) Len() int {
	return len(ns)
}

func (ns NotesByCreated) Swap(i, j int) {
	ns[i], ns[j] = ns[j], ns[i]
}

func (ns NotesByCreated) Less(i, j int) bool {
	return ns[i].Created > ns[j].Created
}
