package domain

import (
	"encoding/json"
	"time"
)

const timestampLayout = "2006-01-02@15:04:05.000"

// Timestamp is a unix timestamp with millisecond precision.
//
// It uses the format "yyyy-mm-dd@hh:mm:ss.xxx" to present the time. The zero
// value is respected, and when printed as a string will print an empty string.
type Timestamp int64

// NewTimestamp creates a Timestamp from a time.Time.
func NewTimestamp(t time.Time) Timestamp {
	if t.IsZero() {
		return 0
	}

	// Store as Unix timestamp with millisecond precision.
	ns := t.UnixNano()
	return Timestamp(ns / 1000000)
}

// NewTimestampFromString creates a Timestamp from its string representation.
func NewTimestampFromString(s string) (Timestamp, error) {
	if s == "" {
		return 0, nil
	}

	parsed, err := time.Parse(timestampLayout, s)
	if err != nil {
		return 0, err
	}

	return NewTimestamp(parsed), nil
}

// Time returns a time.Time from a Timestamp.
func (t Timestamp) Time() time.Time {
	if t == 0 {
		return time.Time{}
	}

	ms := int64(t)
	return time.Unix(0, ms*1000000).UTC()
}

// String implements fmt.Stringer.
func (t Timestamp) String() string {
	// We're going to diverge from the time.Time behavior for a zero value, and
	// instead use the empty string for our zero value time.
	if t == 0 {
		return ""
	}

	return t.Time().Format(timestampLayout)
}

// MarshalJSON implements json.Marshaler.
func (t Timestamp) MarshalJSON() ([]byte, error) {
	return json.Marshal(t.String())
}

// UnmarshalJSON implements json.Unmarshaler.
func (t *Timestamp) UnmarshalJSON(bs []byte) error {
	var s string
	if err := json.Unmarshal(bs, &s); err != nil {
		return err
	}

	parsed, err := NewTimestampFromString(s)
	if err != nil {
		return err
	}

	*t = parsed
	return nil
}
