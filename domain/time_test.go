package domain_test

import (
	"fmt"
	"testing"
	"time"

	"gitlab.com/feidtmb/notes/domain"
)

func TestTimestamp(t *testing.T) {
	cases := []struct {
		name       string
		time       time.Time
		wantString string
	}{
		{
			"Basic time",
			time.Date(2020, time.January, 12, 15, 24, 53, 123999999, time.UTC),
			"2020-01-12@15:24:53.123",
		},
		{
			"Zero time",
			time.Time{},
			"",
		},
	}

	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			stamp := domain.NewTimestamp(c.time)

			if stamp.String() != c.wantString {
				t.Fatalf("Wanted %q, got %q", c.wantString, stamp.String())
			}

			rawJSON, err := stamp.MarshalJSON()
			if err != nil {
				t.Fatal(err)
			}

			if string(rawJSON) != fmt.Sprintf("%q", c.wantString) {
				t.Fatalf("Wanted JSON to match quoted string output, got %q", string(rawJSON))
			}

			unmarshalled := new(domain.Timestamp)
			if err := unmarshalled.UnmarshalJSON(rawJSON); err != nil {
				t.Fatal(err)
			}

			if !unmarshalled.Time().Equal(stamp.Time()) {
				t.Logf("%s", unmarshalled.Time())
				t.Logf("%s", stamp.Time())
				t.Fatal("Expected times to match after parsing as JSON")
			}
		})
	}
}
