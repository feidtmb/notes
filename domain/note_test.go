package domain_test

import (
	"sort"
	"testing"
	"time"

	"gitlab.com/feidtmb/notes/domain"
)

func TestNotesByCreated(t *testing.T) {
	now := time.Now()

	ns := []domain.Note{
		domain.Note{Created: domain.NewTimestamp(now)},
		domain.Note{Created: domain.NewTimestamp(now.Add(time.Hour))},
		domain.Note{Created: domain.NewTimestamp(now.Add(-time.Hour))},
	}

	sort.Sort(domain.NotesByCreated(ns))

	for i := 0; i < len(ns)-1; i++ {
		if ns[i].Created < ns[i+1].Created {
			t.Fatal("Expected notes to be sorted by created descending")
		}
	}
}

func TestNotesByPriority(t *testing.T) {
	ns := []domain.Note{
		domain.Note{Priority: 1},
		domain.Note{Priority: 2},
		domain.Note{Priority: 0},
	}

	sort.Sort(domain.NotesByPriority(ns))

	for i := 0; i < len(ns)-1; i++ {
		if ns[i].Priority < ns[i+1].Priority {
			t.Fatal("Expected notes to be sorted by priority descending")
		}
	}
}
