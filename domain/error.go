package domain

import (
	"encoding/json"
	"net/http"
)

// ErrKind is a kind of error.
type ErrKind string

// Kinds of errors.
const (
	// ErrInternal represents an internal error. It is also the default error
	// kind.
	ErrInternal ErrKind = "internal"

	// ErrRequest represents an error related to an invalid request, which can
	// be resolved by correcting the request. For example, this error may be
	// returned by an HTTP handler that expects a valid JSON body but doesn't
	// receive it.
	ErrRequest ErrKind = "request"

	// ErrNotFound represents an error related to a request for a resource that
	// doesn't exist.
	ErrNotFound ErrKind = "not_found"
)

// Err is an error.
type Err struct {
	// Kind describes the kind of error.
	Kind ErrKind
	// Message is a human-readable message associated with the error.
	Message string
	// Cause is the cause of the error.
	Cause error
}

func (e Err) Error() string {
	if e.Cause != nil {
		return e.Cause.Error()
	}

	switch e.Kind {
	case "", ErrInternal:
		return "internal error"
	case ErrRequest:
		return "request error"
	case ErrNotFound:
		return "not found"
	default:
		return "undefined error"
	}
}

func (e Err) String() string {
	if e.Message != "" {
		return e.Message
	}

	switch e.Kind {
	case ErrRequest:
		return "There's a problem with the request, please try again."
	case ErrNotFound:
		return "Nothing found, the thing you're looking for may have been deleted."
	default:
		return "An error occurred, please try again."
	}
}

// HTTPStatusCode returns the HTTP status code that should be set for HTTP
// responses that return the error.
func (e Err) HTTPStatusCode() int {
	switch e.Kind {
	case ErrRequest:
		return http.StatusBadRequest
	case ErrNotFound:
		return http.StatusNotFound
	default:
		return http.StatusInternalServerError
	}
}

// MarshalJSON marshals the error into JSON format.
//
// Information missing from the error is filled in as appropriate.
func (e Err) MarshalJSON() ([]byte, error) {
	type E struct {
		Kind    ErrKind `json:"kind"`
		Message string  `json:"message"`
	}

	// Use the internal error kind if none is defined.
	kind := ErrInternal
	if e.Kind != "" {
		kind = e.Kind
	}

	return json.Marshal(E{Kind: kind, Message: e.String()})
}
