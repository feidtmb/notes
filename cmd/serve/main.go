// Hosts the notes server.
//
// The following environment variables are inspected to adjust behavior:
//
// - DATABASE_URL: A PostgreSQL connection string for an existing database.
//   Required if running in the live environment. If the database isn't already
//   provisioned, it will be.
// - ENV: Sets the server environment, which affects which database is used.
//   Valid values are "live" and "local", with a postgres database being used
//   for "live" and an in memory database being used for "local".
// - MAILGUN_DOMAIN: Is the domain from which Mailgun email gets sent. It must
//   be a subdomain of URL, because we specify emails as having come from the
//   URL domain.
// - MAILGUN_KEY: An API key for Mailgun. Used to send email in the live
//   environment.
// - PORT: Changes which port the server listens to.
// - URL: Sets the URL used to refer to the server in things like emailed links.
package main

import (
	"context"
	"log"
	"net/http"
	"net/url"
	"os"

	"gitlab.com/feidtmb/lib/crypt"
	"gitlab.com/feidtmb/notes"
	"gitlab.com/feidtmb/notes/database"
	"gitlab.com/feidtmb/notes/database/mem"
	"gitlab.com/feidtmb/notes/database/pg"
	"gitlab.com/feidtmb/notes/mail"
	"gitlab.com/feidtmb/notes/mail/iomail"
	"gitlab.com/feidtmb/notes/mail/mailgun"
)

// DefaultPort is the port the server will be hosted on if the "PORT"
// environment variable isn't specified.
const DefaultPort = "8080"

// Recognized values for the "ENV" environment variable.
const (
	EnvLive  = "live"
	EnvLocal = "local"
)

func main() {
	serve()
	// We treat stopping serving as an error.
	os.Exit(1)
}

func serve() {
	var (
		envDatabaseURL   = os.Getenv("DATABASE_URL")
		envEnv           = os.Getenv("ENV")
		envMailgunDomain = os.Getenv("MAILGUN_DOMAIN")
		envMailgunKey    = os.Getenv("MAILGUN_KEY")
		envPort          = os.Getenv("PORT")
		envURL           = os.Getenv("URL")
	)

	if envURL == "" {
		log.Printf("URL is required.")
		return
	}
	serverURL, err := url.Parse(envURL)
	if err != nil {
		log.Printf("Invalid URL: %v", err)
		return
	}

	var (
		db     database.DB
		mailer mail.Mailer
	)
	switch envEnv {
	case EnvLive:
		pgDB, err := pg.New(envDatabaseURL)
		if err != nil {
			log.Printf("Failed to connect to PostgreSQL database: %v", err)
			return
		}
		defer pgDB.Close()
		if err := pgDB.Up(context.Background()); err != nil {
			log.Printf("Failed run up migration on PostgreSQL database: %v", err)
			return
		}
		db = pgDB

		if envMailgunKey == "" {
			log.Printf("MAILGUN_KEY is required.")
			return
		}
		if envMailgunDomain == "" {
			log.Printf("MAILGUN_DOMAIN is required.")
			return
		}
		mailer = mailgun.New(envMailgunDomain, envMailgunKey)
	case EnvLocal:
		// Seed with test data.
		email := "feidtmb@gmail.com"
		passwordHash, err := crypt.OneWayHash([]byte("password"))
		if err != nil {
			log.Printf("Failed to seed in-memory database: %v", err)
			return
		}
		seed := mem.Seed{
			User: database.NewUser{
				Email:        email,
				PasswordHash: string(passwordHash),
			},
			Notes: []database.NewNote{
				database.NewNote{
					UserEmail: email,
					Content:   "This is an example note.",
				},
				database.NewNote{
					UserEmail: email,
					Content:   "This is a second example note.",
				},
				database.NewNote{
					UserEmail: email,
					Content:   "This is a third example note.",
				},
			},
		}
		db, err = mem.New(&seed)
		if err != nil {
			log.Printf("Failed to initialize in-memory database: %v", err)
			return
		}

		mailer = iomail.New()
	default:
		log.Println("Server environment not specified.")
		return
	}

	server := notes.New(db, serverURL, mailer)

	port := envPort
	if port == "" {
		port = DefaultPort
	}

	log.Printf("Server listening on port %s", port)
	log.Printf("Server shutdown: %v", http.ListenAndServe(":"+port, server))
}
