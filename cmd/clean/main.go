// Cleans the notes PostgreSQL database.
//
// The following environment variables are inspected to adjust behavior:
//
// - DATABASE_URL: A PostgreSQL connection string for an existing database.
//   Required.
package main

import (
	"context"
	"log"
	"os"

	"gitlab.com/feidtmb/notes/database/pg"
)

func main() {
	var envDatabaseURL = os.Getenv("DATABASE_URL")

	db, err := pg.New(envDatabaseURL)
	if err != nil {
		log.Printf("Failed to connect to PostgreSQL database: %v", err)
		return
	}
	defer db.Close()

	if err := db.DeleteExpiredKeys(context.Background()); err != nil {
		log.Printf("Error deleting expired keys: %v", err)
		return
	}

	log.Println("Expired keys deleted")
}
