// +heroku goVersion go1.16
// +heroku install ./cmd/...

module gitlab.com/feidtmb/notes

go 1.16

require (
	github.com/go-chi/chi v4.1.2+incompatible
	github.com/google/uuid v1.3.0 // indirect
	github.com/json-iterator/go v1.1.12 // indirect
	github.com/lib/pq v1.10.3
	github.com/mailgun/mailgun-go/v4 v4.5.3
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/pborman/uuid v1.2.1
	github.com/pkg/errors v0.9.1 // indirect
	gitlab.com/feidtmb/lib v1.0.7
	golang.org/x/crypto v0.0.0-20210921155107-089bfa567519 // indirect
)
